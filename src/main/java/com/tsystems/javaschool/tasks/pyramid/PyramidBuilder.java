package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int count = 0;
        int size = inputNumbers.size();

        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        while (size != 0) {
            size -= ++count;

            if (size < 0)
                throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        int[][] arr = new int[count][2 * count - 1];

        LinkedList<Integer> integerLinkedList = new LinkedList<>(inputNumbers);
        int complitedIterations = 0;
        while (!integerLinkedList.isEmpty()) {
            for (int i = 0; i < count; i++)
                arr[count - 1][arr[count - 1].length - 1 - complitedIterations - 2 * i] = integerLinkedList.remove(integerLinkedList.size() - 1);

            complitedIterations++;
            count--;
        }

        return arr;
    }


}
