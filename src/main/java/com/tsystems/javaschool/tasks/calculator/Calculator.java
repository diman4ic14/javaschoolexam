package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    public static void main(String[] args) {
        System.out.println(new Calculator().evaluate("10-(2-7+3)+4"));
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            if (statement.isEmpty())
                throw new InvalidInputDataException();

            int countOpeningBrackets = getCountBrackets(statement, "(");
            int countClosingBrackets = getCountBrackets(statement, ")");

            if (countOpeningBrackets != countClosingBrackets)
                throw new InvalidInputDataException();

            Stack<String> stack = new Stack<>();
            char[] chars = statement.toCharArray();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < chars.length; i++) {
                if (chars[i] >= '0' && chars[i] <= '9' || chars[i] == '.') {
                    sb.append(chars[i]);

                    if (i == chars.length - 1)
                        stack.push(sb.toString());

                }
                else {
                    if (sb.length() > 0) {
                        stack.push(sb.toString());
                        sb = new StringBuilder();
                    }

                    if (chars[i] != ')')
                        stack.push(new String(new char[]{chars[i]}));
                    else
                        stack.push(String.valueOf(calculate(stack)));
                }
            }

            double result = calculate(stack);

            if (result == Math.round(result))
                return new BigDecimal(result).setScale(0, RoundingMode.UP).toString();

            String formatting = new BigDecimal(result).setScale(4, RoundingMode.HALF_UP).toString();

            while (formatting.endsWith("0"))
                formatting = formatting.substring(0, formatting.length() - 1);


            return formatting;
        } catch (NullPointerException | NumberFormatException | InvalidInputDataException e) {
            return null;
        }
    }


    private double calculate(Stack<String> stack) throws NumberFormatException {
        List<String> list = new ArrayList<>();
        while (!stack.isEmpty()) {
            String elem = stack.pop();
            if (elem.equals("("))
                break;
            else
                list.add(0, elem);
        }

        double temp = 0;

        while (list.contains("*") || list.contains("/")) {
            int indexMultiplying = list.indexOf("*");
            int indexDividing = list.indexOf("/");

            if (indexMultiplying < indexDividing) {
                calculateMultiplyingOrDividing(list, "*");
                calculateMultiplyingOrDividing(list, "/");
            }
            else {
                calculateMultiplyingOrDividing(list, "/");
                calculateMultiplyingOrDividing(list, "*");
            }
        }

        temp += Double.parseDouble(list.get(0));

        for (int i = 2; i < list.size(); i = i + 2) {
            switch (list.get(i - 1)) {
                case "+":
                    temp += Double.parseDouble(list.get(i));
                    break;
                case "-":
                    temp -= Double.parseDouble(list.get(i));
                    break;
                default:
                    throw new NumberFormatException();
            }
        }

        return temp;
    }

    private void calculateMultiplyingOrDividing(List<String> list, String operator) throws NumberFormatException {
        if (list.contains(operator)) {
            int index = list.indexOf(operator);
            double calculate;

            if (operator.equals("*"))
                calculate = Double.parseDouble(list.get(index - 1)) * Double.parseDouble(list.get(index + 1));
            else
                calculate = Double.parseDouble(list.get(index - 1)) / Double.parseDouble(list.get(index + 1));

            for (int i = 0; i < 3; i++)
                list.remove(index - 1);

            list.add(index - 1, Double.toString(calculate));
        }
    }


    private int getCountBrackets(String statement, String bracket) {
        int count = 0;
        while (statement.contains(bracket)) {
            count++;
            statement = statement.substring(statement.indexOf(bracket) + 1);
        }

        return count;
    }
}
